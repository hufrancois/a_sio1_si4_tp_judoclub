
package Exercice;

import donnees.Dao;
import entites.Club;
import java.util.Scanner;

public class Essai1 {

    public static void main(String[] args) {
        // TODO code application logic here
                Scanner clavier= new Scanner(System.in); 
        String codeClubSaisi; 
        
        System.out.print("Entrer le code du club à afficher: ");
        codeClubSaisi=clavier.next();
        System.out.println();
        
        Club club= Dao.getClubDeCode(codeClubSaisi);
        
        System.out.println("Nom du Club     : "+club.getNomClub());
        System.out.println("Adresse du Club : "+club.getAdrClub());
        System.out.println("Nombre de victoires du club : "+club.nbTotalVictoires());
        System.out.println("L'age moyen des judokas du club : "+club.ageMoyen());
        System.out.println("Le poids moyen des judokas féminin du club : "+club.poidsMoyen("F"));
        System.out.println("Le poids moyen des judokas masculin du club : "+club.poidsMoyen("M"));
    }
}
