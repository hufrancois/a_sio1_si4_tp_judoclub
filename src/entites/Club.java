package entites;

import java.util.LinkedList;
import java.util.List;

public class Club {
   
  //<editor-fold defaultstate="collapsed" desc="Attributs privés">
    
    private String         codeClub;
    
    private String         nomClub;
    private String         adrClub;
    
    private List<Judoka>   lesJudokas= new LinkedList();
    
    //</editor-fold>
  
  //<editor-fold defaultstate="collapsed" desc="Constructeurs">
    
    public Club() { }
    
    public Club(String codeClub, String nomClub, String adrClub) {
        
        this.codeClub = codeClub;
        this.nomClub  = nomClub;
        this.adrClub  = adrClub;
    }
    
    //</editor-fold>
   
  //<editor-fold defaultstate="collapsed" desc="Gets Sets">
  
  public String getCodeClub() {
      return codeClub;
  }
  
  public void setCodeClub(String codeClub) {
      this.codeClub = codeClub;
  }
  
  public String getNomClub() {
      return nomClub;
  }
  
  public void setNomClub(String nomClub) {
      this.nomClub = nomClub;
  }
  
  public String getAdrClub() {
      return adrClub;
  }
  
  public void setAdrClub(String adrClub) {
      this.adrClub = adrClub;
  }
  
  public List<Judoka> getLesJudokas() {
        return lesJudokas;
    }

  public void setLesJudokas(List<Judoka> lesJudokas) {
        this.lesJudokas = lesJudokas;
  }
  
  
  //</editor-fold>

  //<editor-fold defaultstate="collapsed" desc="Méthodes publiques">
  
  public void affichageConsole(){
      
      System.out.printf("%-8s %-20s %-40s", codeClub,nomClub,adrClub);
  }
  
  public int nbTotalVictoires(){
      
      int nbtotvic=0;
      for(Judoka j : lesJudokas){
          nbtotvic+=j.getNbVictoires();
      }
      
      return nbtotvic;
      
  };
  
  public Float ageMoyen(){
      
      float agemoy=0;
      int c=0;
      for(Judoka j : lesJudokas){
          agemoy+=j.getAge();
          c++;
      }
      
      agemoy=agemoy/c;
      return agemoy;
      
  };
          
  public Float poidsMoyen(String pSexe){
      
      float poidsmoy=0;
      int c=0;
      for(Judoka j : lesJudokas){
          
          if(j.getSexe().equals(pSexe)){
          poidsmoy+=j.getPoids();
          c++;
          }
      }
      
      poidsmoy=poidsmoy/c;
      return poidsmoy;
      
  };
          
  
  //</editor-fold>
}
