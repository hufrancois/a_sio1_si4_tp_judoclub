package entites;
import java.util.Date;
import static utilitaires.UtilDate.dateVersChaine;
import static utilitaires.UtilDate.ageEnAnnees;
import static utilitaires.UtilDojo.determineCategorie;

public class Judoka  {
    
   //<editor-fold defaultstate="collapsed" desc="Attributs privés">
  
    private Long      id; 
    private String    nom;
    private String    prenom;
    private String    sexe;
    private Date      dateNaiss;
    private int       poids;
    private String    ville;
    private int       nbVictoires;
    
    private Club      leClub;
    
    //</editor-fold>
    
   //<editor-fold defaultstate="collapsed" desc="Constructeurs">
   
   public Judoka() {}

   public Judoka(Long id, String nom, String prenom, String sexe, Date dateNaiss, 
                 int poids, String ville, int nbVictoires
                ) 
   {
        this.id          = id;
        this.nom         = nom;
        this.prenom      = prenom;
        this.sexe        = sexe;
        this.dateNaiss   = dateNaiss;
        this.poids       = poids;
        this.ville       = ville;
        this.nbVictoires = nbVictoires;
    }
   
   public Judoka( Long   id,String nom,   String prenom, String sexe, Date dateNaiss, 
                  int    poids, String ville,  int nbVictoires,
                  Club   leClub
                ) 
   {   
       this.id          = id;
       this.nom         = nom;
       this.prenom      = prenom;
       this.sexe        = sexe;
       this.dateNaiss   = dateNaiss;
       this.poids       = poids;
       this.ville       = ville;
       this.nbVictoires = nbVictoires;
       this.leClub      = leClub;
   }
   
   //</editor-fold>
   
   //<editor-fold defaultstate="collapsed" desc="Getters et setters">
    
    public Long   getId() { return id;}
    public void   setId(Long id) {this.id = id;}
 
    public String getNom() { return nom;}   
    public void   setNom(String nom) { this.nom = nom;}
    
    public String getPrenom() {
        return prenom;
    }
    public String getSexe() {
        return sexe;
    }
    public void   setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public void   setSexe(String sexe) {
        this.sexe = sexe;
    }
    public Date   getDateNaiss() {
        return dateNaiss;
    }
    public void   setDateNaiss(Date dateNaiss) {
        this.dateNaiss = dateNaiss;
    }
    public int    getPoids() {
        return poids;
    }
    public void   setPoids(int poids) {
        this.poids = poids;
    }
    public String getVille() {
        return ville;
    }
    public void   setVille(String ville) {
        this.ville = ville;
    }
    public int    getNbVictoires() {
        return nbVictoires;
    }
    public void   setNbVictoires(int nbVictoires) {
        this.nbVictoires = nbVictoires;
    }
   
     public Club getLeClub() {
        return leClub;
    }

    public void setLeClub(Club leClub) {
        this.leClub = leClub;
    }
   
    //</editor-fold>
   
   //<editor-fold defaultstate="collapsed" desc="Méthodes">
    
    public void affichageConsole(){
        
        String format="%-10s %-10s %-2s %-10s %-10s %-15s %4d kg %-10s %3d Victoires";
        System.out.printf(format,
                          nom,
                          prenom,
                          sexe,
                          dateVersChaine(dateNaiss),
                          ageEnAnnees(dateNaiss),
                          ville,
                          poids,
                          determineCategorie(sexe,poids),
                          nbVictoires
        );
    }
    
    public int getAge() {
        
        return ageEnAnnees(dateNaiss);
    }
    
    public String getCategories() {
        
        return determineCategorie(sexe,poids);
    }
    
    //</editor-fold> 
   /* 
   //<editor-fold defaultstate="collapsed" desc="Redéfinition de la méthode toString">
    
    @Override
    public String toString() {
        
        return nom + " " + prenom + " " + nbVictoires + " victoires";
    }
    //</editor-fold>
   */    
}


