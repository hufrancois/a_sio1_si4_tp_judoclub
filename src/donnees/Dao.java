package donnees;

import entites.Club;
import entites.Judoka;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import static utilitaires.UtilDate.chaineVersDate;

public class Dao {
      
 private  static  List<Club>   tousLesClubs   = new LinkedList();    
 private  static  List<Judoka> tousLesJudokas = new LinkedList();

 //<editor-fold defaultstate="collapsed" desc="Code créant les données">
  
 static {
 
  Club   c1,c2;
  
  c1 = new Club("RCAJC","RCA Judo Club","12 Rue Ippon 62000 Arras");
  c2 = new Club("JCL","Judo Club Lensois","15 Rue de la Gayette 62498 Lens");
 
  tousLesClubs.addAll(Arrays.asList(c1,c2)); 
  
  Judoka p01, p02, p03, p04, p05, p06, p07, p08, p09, p10, p11, p12, p13, p14, p15;
 
  p01= new Judoka(101L,"Durant",    "Pierre",      "M",chaineVersDate("12/05/1993"), 83, "Arras",  9,c1);
  p02= new Judoka(102L,"Martin",    "Sophie",      "F",chaineVersDate("25/11/1991"), 52, "Lens",   6,c2);
  p03= new Judoka(103L,"Lecoutre",  "Thierry",     "M",chaineVersDate("05/08/1992"), 72, "Arras",  5,c1);
  p04= new Judoka(104L,"Duchemin",  "Fabienne",    "F",chaineVersDate("14/03/1992"), 61, "Lens",   10,c2);    
  p05= new Judoka(105L,"Duchateau", "Jacques",     "M",chaineVersDate("18/07/1992"), 91, "Bapaume",4,c1) ;     
  p06= new Judoka(106L,"Lemortier", "Laurent",     "M",chaineVersDate("18/02/1989"), 76, "Arras",1,c1);
  p07= new Judoka(107L,"Dessailles","Sabine",      "F",chaineVersDate("23/03/1990"), 68, "Arras",3,c1); 
  p08= new Judoka(108L,"Bataille",  "Boris",       "M",chaineVersDate("17/11/1991"), 102,"Vitry-En-Artois",7,c1);
  p09= new Judoka(109L,"Lerouge",   "Laëtitia",    "F",chaineVersDate("09/10/1992"), 46, "Lens",4,c2);
  p10= new Judoka(110L,"Renard",    "Paul",        "M",chaineVersDate("16/08/1992"), 68, "Lens",9,c2); 
  p11= new Judoka(111L,"Durant",    "Jacques",     "M",chaineVersDate("13/04/1990"), 75, "Arras",4,c1); 
  p12= new Judoka(112L,"Delespaul", "Martine",     "F",chaineVersDate("25/02/1991"), 55, "Lens",6,c2);
  p13= new Judoka(113L,"Leblanc",   "Jean-Pierre", "M",chaineVersDate("03/12/1992"), 78, "Achicourt",8,c1);
  p14= new Judoka(114L,"Notame",    "Béatrice",    "F",chaineVersDate("17/09/1991"), 55, "Méricourt",9,c2);
  p15= new Judoka(115L,"Leroux",    "Bertrand",    "M",chaineVersDate("05/10/1991"), 82, "Liévin",7,c2);
  
  tousLesJudokas.addAll(Arrays.asList(p01, p02, p03, p04, p05, p06, p07, p08, p09, p10, p11, p12, p13, p14, p15));

  c1.getLesJudokas().addAll(Arrays.asList(p01,p03,p05,p06,p07,p08,p11,p13)); 
  c2.getLesJudokas().addAll(Arrays.asList(p02,p04,p09,p10,p12,p14,p15));
 
}
 
  //</editor-fold>
   
 //<editor-fold defaultstate="collapsed" desc="Méthodes d'accès aux données">
 
 public static List<Judoka> getTousLesJudokas() {
    
     return tousLesJudokas;
 }
 
 public static List<Club>   getTousLesClubs() {
     return tousLesClubs;
 }
 
 public static Judoka       getJudokaNumero(Long pId){
     
     Judoka judoka=null;
     
     for(Judoka unJudoka: tousLesJudokas){
         
         if( unJudoka.getId().equals(pId) ){ judoka=unJudoka;break;}
     }
     
     return judoka;
 }
 
 public static Club         getClubDeCode (String pCode){
     
     Club club=null;
     
     for(Club unClub: tousLesClubs){
         
         if( unClub.getCodeClub().equals(pCode) ){ club=unClub;break;}
     }
     
     return club;
 }
 //</editor-fold>
 
}
