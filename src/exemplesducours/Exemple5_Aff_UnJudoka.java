package exemplesducours;

import static donnees.Dao.getJudokaNumero;
import entites.Judoka;
import java.util.Scanner;
import static utilitaires.UtilDate.ageEnAnnees;

public class Exemple5_Aff_UnJudoka {

    public static void main( String[ ] args) {

        Scanner clavier   = new Scanner(System.in);
        
        System.out.print("\nIdentifiant du Judoka à afficher? ");
        Long  idJdkSaisi = clavier.nextLong(); 
        
        Judoka jdk=getJudokaNumero(idJdkSaisi);
        
        System.out.println();
        jdk.affichageConsole();
        System.out.println();
        
        String debutPhrase=jdk.getSexe().equals("M")?" Il ":" Elle ";
        
        int age=ageEnAnnees(jdk.getDateNaiss());
        
        System.out.println(debutPhrase+"a "+age+" ans\n");
        
        System.out.println();
    }
}


