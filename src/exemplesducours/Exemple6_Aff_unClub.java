
package exemplesducours;

import donnees.Dao;
import entites.Club;
import java.util.Scanner;

public class Exemple6_Aff_unClub {

    public static void main(String[] args) {
        
        Scanner clavier= new Scanner(System.in); 
        String codeClubSaisi; 
        
        System.out.print("Entrer le code du club à afficher: ");
        codeClubSaisi=clavier.next();
        System.out.println();
        
        Club club= Dao.getClubDeCode(codeClubSaisi);
        
        System.out.println("Nom du Club     : "+club.getNomClub());
        System.out.println("Adresse du Club : "+club.getAdrClub());
        System.out.println();    
    }
}

