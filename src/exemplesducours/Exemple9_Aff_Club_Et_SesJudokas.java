
package exemplesducours;

import static donnees.Dao.getClubDeCode;
import entites.Club;
import entites.Judoka;
import java.util.Scanner;

public class Exemple9_Aff_Club_Et_SesJudokas {

    public static void main(String[] args) {
        
         String decalage="      ";
        
         Scanner clavier= new Scanner(System.in); 
         
         String codeClubSaisi; 
         
         System.out.print(decalage+"Entrer le code du club à afficher: ");
         codeClubSaisi=clavier.next();
         System.out.println();
        
         Club club= getClubDeCode(codeClubSaisi);
        
         System.out.println(decalage+"Nom du Club     : "+club.getNomClub());
         System.out.println(decalage+"Adresse du Club : "+club.getAdrClub());
         System.out.println();
         
         for (Judoka jdk : club.getLesJudokas()){
         
            System.out.print(decalage); 
            jdk.affichageConsole();
           
            System.out.println();
         }
         System.out.println();        
    }
}


