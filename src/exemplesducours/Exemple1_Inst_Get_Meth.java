
package exemplesducours;

import entites.Judoka;
import static utilitaires.UtilDate.chaineVersDate;

public class Exemple1_Inst_Get_Meth {

    public static void main(String[] args) {
                
        Judoka jdk= new Judoka(505L,"Delassus","Pierre","M",chaineVersDate("12/10/1995"),80,"Arras",12);    
           
        System.out.println("Affichage 1\n");
        System.out.println(jdk);
        System.out.println();
        
        System.out.println("Affichage 2\n");
        System.out.println(jdk.getNom());
        System.out.println(jdk.getPrenom());
        System.out.println();  
        
        System.out.println("Affichage 3\n");
        afficherJudoka(jdk);
        System.out.println("\n");
        
        
        System.out.println("Affichage 4\n");
        jdk.affichageConsole();
        System.out.println("\n");
    }
    
    static void afficherJudoka(Judoka judoka) {
       
        String format="%-15s %-15s %3d kg\n";
        System.out.printf(format,judoka.getNom(),judoka.getPrenom(),judoka.getPoids() );
    }
}
