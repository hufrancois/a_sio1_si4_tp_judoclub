package exemplesducours;

import static donnees.Dao.getJudokaNumero;
import entites.Judoka;
import static utilitaires.UtilDate.ageEnAnnees;

public class Exemple7_Aff_JudokaEtSonClub {

    public static void main( String[ ] args) {

        Judoka judoka=getJudokaNumero(101L);
        
        System.out.println();
        judoka.affichageConsole();
        System.out.println();
        
        String debutPhrase=judoka.getSexe().equals("M")?" Il ":" Elle ";
        
        int age=ageEnAnnees(judoka.getDateNaiss());
        
        System.out.println(debutPhrase+"a "+age+" ans\n");
        
        System.out.println(" Son club est le "+ judoka.getLeClub().getNomClub());

        System.out.println();
    }
}


