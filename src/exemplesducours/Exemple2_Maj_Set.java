package exemplesducours;

import entites.Judoka;
import static utilitaires.UtilDate.chaineVersDate;

public class Exemple2_Maj_Set {
    
    public static void main(String[] args) {
        
       Judoka jdk;
       
       jdk= new Judoka(101L,"Durant","Pierre","M", chaineVersDate("12/05/1993"), 83,"Arras",9);
      
       System.out.println("Affichage du judoka avant mise à jour du poids\n");
        
       jdk.affichageConsole();
       System.out.println("\n");
       
       jdk.setPoids(81);
    
       
       System.out.println("Affichage du judoka après mise à jour du poids\n");
       jdk.affichageConsole();
       System.out.println("\n");      
    }

}

